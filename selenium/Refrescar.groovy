package actions.selenium

import actions.selenium.Navegador

class Refrescar{
  
  	public void run(def params){
    	try {
    		Navegador.Driver.navigate().refresh()
    		println("Se ha recargado el navegador.");	
    	}
    	catch(Exception e) {
    		println("No se pudo recargar el navegador. ${e}");
    		assert false,"No se pudo recargar el navegador. ${e}"
    	}
  	}
}