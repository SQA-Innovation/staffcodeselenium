package actions.selenium

import actions.selenium.Navegador

class VerificarTitulo{
  
	public void run(def params){
	  	if (!params.Titulo.equals(Navegador.Driver.getTitle())) {
	  		println("Error: El titulo de la pagina ${Navegador.Driver.getTitle()} no coincide con el titulo esperado: ${params.Titulo}");
	  		assert false ,"Error: El titulo de la pagina ${Navegador.Driver.getTitle()} no coincide con el titulo esperado: ${params.Titulo}"
	  	}else{
	  		println("El titulo de la pagina ${Navegador.Driver.getTitle()} coincide con el titulo esperado: ${params.Titulo}");
	  	}
	}
}