package actions.selenium

import actions.selenium.Navegador


class CambiarVentana{

    public run(def params){
        int iTimeout = 20

        if(params."Browser window name" == "Ventana Default"){
            Navegador.Driver.switchTo().window(Navegador.MainWinHandle)
            return
        }   

        while(iTimeout > 0){
            for (handle in Navegador.Driver.getWindowHandles()){
                Navegador.Driver.switchTo().window(handle)
                if (Navegador.Driver.getTitle() == params."Nombre ventana"){
                    return;
                }
            }
            iTimeout--
            sleep(1000)
        }
        println("La ventana: "+params."Nombre ventana"+" no existe.");
        assert false,"La ventana: ${params."Nombre ventana"} no existe."
    }
}