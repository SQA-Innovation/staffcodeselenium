package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement
import org.openqa.selenium.JavascriptExecutor

class ObtenerTexto{
  
  public String run(def params){
    WebElement element = Elements.find(params,Navegador.Driver)
    return element.getText()
  }
}