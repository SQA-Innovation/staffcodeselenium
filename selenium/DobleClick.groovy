package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions


class DobleClick{
  
	public void run(def params){
	    WebElement element = Elements.find(params,Navegador.Driver)
	    
	    Actions action = new Actions(Navegador.Driver)
	  	try {
	  		action.doubleClick(element)
	  		action.perform()	
	  		println("Se ha realizado doble click sobre un elemento");
	  	}
	  	catch(Exception e) {
	  		println("Se ha producido un error: Debe elegir un tipo de Scroll válido. "+e); 
            assert false,"Se ha producido un error: Debe elegir un tipo de Scroll válido. ${e}"
	  	}
	}
}