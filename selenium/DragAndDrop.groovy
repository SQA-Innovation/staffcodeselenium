package actions.selenium

import actions.selenium.utils.Elements
import org.openqa.selenium.JavascriptExecutor
import actions.selenium.Navegador
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions


class DragAndDrop{
  
  public void run(def params){
    WebElement element = Elements.find(["ID":params."From ID","Tipo ID":params."From ID Type"],Navegador.Driver)    
    WebElement target = Elements.find(["ID":params."To ID","Tipo ID":params."To ID Type"],Navegador.Driver)
    
    Actions action = new Actions(Navegador.Driver)
  	try {
  		action.dragAndDrop(element,target).build().perform()
  		println("Se ha realizado un Drag & Drop sobre un elemento. ");	
  	}
  	catch(Exception e) {
  		println("No se pudo realizar el Drag & Drop al elemento. "+e);
  		assert false, "No se pudo realizar el Drag & Drop al elemento. ${e}"
  	}
  	
  } 
}