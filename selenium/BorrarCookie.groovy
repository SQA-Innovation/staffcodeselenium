package actions.selenium

import actions.selenium.Navegador

class BorrarCookie{
	public void run(def params){
	    try{
          	if(params."Nombre Cookie" == "ALL"){
              	Navegador.Driver.manage().deleteAllCookies()
              	println("Se han eliminado todas las cookies.");  
          	}
          	else{
              	Navegador.Driver.manage().deleteCookieNamed(params."Nombre Cookie")
              	println("Se ha eliminado la cookie ${params."Nombre Cookie"}.");
          	}
	    }catch(Exception e){
	        println("No se pudo eliminar la(s) cookie(s). "+e);
	        assert false, "No se pudo eliminar la(s) cookie(s). ${e}"
	    }
	}
}