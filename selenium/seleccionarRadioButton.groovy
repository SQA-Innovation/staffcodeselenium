package actions.selenium;

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement

class seleccionarRadioButton{
    public void run(def params){
        boolean bValue;
		WebElement element = Elements.find(params,Navegador.Driver)
		bValue = element.isSelected();
		
		if (bValue != true){
			element.click();
			println("Se ha marcado un Radiobutton.");
		}else{
			println("El Radiobutton '${params."ID"}' ya se encuentra marcado");
			assert false,"El Radiobutton '${params."ID"}' ya se encuentra marcado"
		}
    }
}