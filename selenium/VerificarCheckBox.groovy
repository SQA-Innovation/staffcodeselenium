package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement

class VerificarCheckBox{
  
	public void run(def params){
	    WebElement element = Elements.find(params,Navegador.Driver)
	    if (element.selected != params."Esta Checkeado") {
	    	println("Error en check box, se espera que el estado controlado sea: ${params."Esta Checkeado"}");
	    	assert false, "Error en check box, se espera que el estado controlado sea: ${params."Esta Checkeado"}"
	    }else{
	    	println("El estado esperado coincide con el estado actual del elemento.");
	    }
	}
}