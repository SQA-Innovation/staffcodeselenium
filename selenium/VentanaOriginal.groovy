package actions.selenium;

import actions.selenium.Navegador

import java.util.ArrayList
import java.util.List
import java.util.Set

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.OutputType
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

class VentanaOriginal{
    
    public static String ventanaPadre;
    
    public run(def params){
        ventanaPadre = Navegador.Driver.getWindowHandle();
		ArrayList<String> Ventanas = new ArrayList<String> (Navegador.Driver.getWindowHandles())
		
		if(Ventanas.size() > 1){
			for (handle in Navegador.Driver.getWindowHandles()){
                Navegador.Driver.switchTo().window(handle)
                return;
              }
            println("Se ha movido a la ventana original.");
		}else{
            assert false,"El foco ya se encuentra en la ventana principal."
		}
	}
}