package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.JavascriptExecutor

class VerificarItemSeleccionado{
  
    public void run(def params){
        WebElement element = Elements.find(params,Navegador.Driver)
          
        def option = new Select(element).getFirstSelectedOption()
        def js = (JavascriptExecutor) Navegador.Driver
          
        String selectedLabel = option.getText()
        String selectedValue = option.getAttribute("value")
        String selectedIndex = option.getAttribute("index")
          
        if(params."Item Label"){
            if (!selectedLabel.equals(params."Item Label")) {
                println("Error: La opcion seleccionada ${selectedLabel} no coincide con la opcion esperada: ${params."Item Label"}");
                assert false, "Error: La opcion seleccionada ${selectedLabel} no coincide con la opcion esperada: ${params."Item Label"}"
            }else{
                println("La opcion seleccionada ${selectedLabel} coincide con la opcion esperada: ${params."Item Label"}");
            }  
        }
        
        if(params."Item Value"){
            if (!selectedValue.equals(params."Item Value")) {
                println("Error: La opcion seleccionada ${selectedValue} no coincide con la opcion esperada: ${params."Item Value"}");
                assert false, "Error: La opcion seleccionada ${selectedValue} no coincide con la opcion esperada: ${params."Item Value"}"
            }else{
                println("La opcion seleccionada ${selectedValue} coincide con la opcion esperada: ${params."Item Value"}");
            } 
        }
          
        if(params."Item Index"){
            if (!selectedIndex.equals(params."Item Index")) {
                println("Error: La opcion seleccionada ${selectedIndex} no coincide con la opcion esperada: ${params."Item Index"}");
                assert false, "Error: La opcion seleccionada ${selectedIndex} no coincide con la opcion esperada: ${params."Item Index"}"
            }else{
                println("La opcion seleccionada ${selectedIndex} coincide con la opcion esperada: ${params."Item Index"}");
            }
        }
    }
}