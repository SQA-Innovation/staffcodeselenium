package actions.selenium

import actions.selenium.Navegador


class VerificarURL{
  
  	public void run(def params){
	    if(params.URL){
	    	if (params.URL.equals(Navegador.Driver.getCurrentUrl())) {
	    		println("Error:La URL esperada ${params.URL} no coincide con la URL desplegada: ${Navegador.Driver.getCurrentUrl()}");
	    		assert false , "Error:La URL esperada ${params.URL} no coincide con la URL desplegada: ${Navegador.Driver.getCurrentUrl()}"
	    	}else{
	    		println("La URL esperada ${params.URL} coincide con la URL desplegada: ${Navegador.Driver.getCurrentUrl()}");
	    	}
	    }
	    
	    if(params."Expresion Regular"){
	      	assert Navegador.Driver.getCurrentUrl() =~ /${params."Expresion Regular"}/, "Error: La expresión regular esperada ${params."Expresion Regular"} no coincide con la URL desplegada: ${Navegador.Driver.getCurrentUrl()}"
	    }
  	}
}