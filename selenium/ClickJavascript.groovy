package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.JavascriptExecutor

class ClickJavascript{
  
    public void run(def params){

    WebElement element = Elements.find(params,Navegador.Driver)
   
    try{
        JavascriptExecutor executor = (JavascriptExecutor) Navegador.Driver
	    executor.executeScript("arguments[0].click();", element)
        println("Se ha hecho click javascript en el elemento "+params."Tipo ID"+":"+params.ID);
    }catch(org.openqa.selenium.WebDriverException error){
        iTimeout--
        if(error.message.contains("No se pudo hacer click sobre el elemento.")){
            if(iTimeout == 0){
                throw error
            }
        }
        else{
            throw error
        }
        sleep(1000)
    }           
  }
}