package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement

class EnviarFormulario{
	public void run(def params){
	    WebElement element = Elements.find(params,Navegador.Driver)
	    try {
	    	element.submit()
	    	println("Se ha enviado un formulario.");	
	    }
	    catch(Exception e) {
	    	println("No se pudo enviar el formulario.");
	    	assert false,"No se pudo enviar el formulario."
	    }
	    
	}
}