package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement
import org.openqa.selenium.JavascriptExecutor

class VerificarTexto{

    public static String Valor;
  
    public void run(def params){
        WebElement element = Elements.find(params,Navegador.Driver)
        if (!element.getText().equals(params.Texto)) {
            println("Error, el texto esperado '${params.Texto}', no coincide con el texto '${element.getText()}' en el elemento ${params.ID}");
            assert false, "Error, el texto esperado '${params.Texto}', no coincide con el texto '${element.getText()}' en el elemento ${params.ID}"
        }else{
            println("El texto esperado '${params.Texto}', coincide con el texto '${element.getText()}' en el elemento ${params.ID}");
        }
    }
    
   public void CompararValue(def params){
        WebElement element = Elements.find(params,Navegador.Driver)
    	String textoActual = element.getAttribute("value")
       	def js = (JavascriptExecutor) Navegador.Driver
    	
        if (!textoActual.contentEquals(params.Texto)){
            println("Error, el texto esperado '${params.Texto}', no coincide con el texto '${textoActual}' en el elemento ${params.ID}");
            assert false, "Error, el texto esperado '${params.Texto}', no coincide con el texto '${textoActual}' en el elemento ${params.ID}"
        } else{
            println("El texto esperado '${params.Texto}', coincide con el texto '${textoActual}' en el elemento ${params.ID}");
        }
    }

     public void encontrarTexto(def params){
        WebElement element = Elements.find(params,Navegador.Driver)
        String textoActual = element.getText();
        boolean resultado = textoActual.contains(params.Texto);
        if (!resultado){
            println("Error, el texto esperado '${params.Texto}', no fue encontrado en el texto '${textoActual}' en el elemento ${params.ID}");
       		assert false, "Error, el texto esperado '${params.Texto}', no fue encontrado en el texto '${textoActual}' en el elemento ${params.ID}"
       	}else{
            println("El texto esperado '${params.Texto}', fue encontrado en el texto '${textoActual}' en el elemento ${params.ID}");
        }
    }     
    
    public void capturarValorEnRango(def params){
        WebElement element = Elements.find(params,Navegador.Driver)
        String textoActual = element.getText();
        if(params."Inicio"){
            int inicio = Integer.parseInt(params."Inicio");
            if(params."Fin"){
                int fin =    Integer.parseInt(params."Fin");
                String texto = textoActual.substring(inicio,fin);
            }else{
                String texto = textoActual.substring(inicio);
            }
            Valor = texto
            println("Se ha capturado el texto ${Valor} del elemento ${params.ID}.");
        }else{
            println("Debes indicar la posicion del caracter inicial.");
            assert false, "Debes indicar la posicion del caracter inicial."
        }
    }
    
    public void capturarValor(def params) {
        try {
            WebElement element = Elements.find(params,Navegador.Driver)
            Valor = element.getAttribute("innerHTML");
            println("Se ha capturado el texto ${Valor} del elemento ${params.ID}.");
        } catch(Exception e){
            println("No fue posible capturar el texto ${e}");
            assert false, "No fue posible capturar el texto ${e}"
        }
    }

    public void capturarValorHidden(def params) {
        def js = (JavascriptExecutor) Navegador.Driver
        switch (params."Tipo ID"){
            case "ID":
                try {
                    WebElement element = Elements.find(params,Navegador.Driver)
                    js.executeScript("document.getElementById('${params.ID}').setAttribute('type', 'text');")
                    Valor = element.getAttribute("value");
                    js.executeScript("document.getElementById('${params.ID}').setAttribute('type', 'hidden');")
                    println("Se ha capturado el texto ${Valor} desde un campo oculto");
                } catch(Exception e){
                    println("No fue posible capturar el texto. ${e}");
                    assert false, "No fue posible capturar el texto. ${e}"
                }
            break
            case "Tag Name":
                try {
                    WebElement element = Elements.find(params,Navegador.Driver)
                    js.executeScript("document.getElementsByTagName('${params.ID}')[${params.Index.toInteger()}].setAttribute('type', 'text');")
                    Valor = element.getAttribute("value");
                    js.executeScript("document.getElementsByTagName('${params.ID}')[${params.Index.toInteger()}].setAttribute('type', 'hidden');")
                    println("Se ha capturado el texto ${Valor} desde un campo oculto");
                } catch(Exception e){
                    println("No fue posible capturar el texto. ${e}");
                    assert false, "No fue posible capturar el texto. ${e}"
                }
            break
            default:
               try {
                    WebElement element = Elements.find(params,Navegador.Driver)
                    js.executeScript("document.getElementById('${params.ID}').setAttribute('type', 'text');")
                    Valor = element.getAttribute("value");
                    js.executeScript("document.getElementById('${params.ID}').setAttribute('type', 'hidden');")
                    println("Se ha capturado el texto ${Valor} desde un campo oculto");
                } catch(Exception e){
                    println("No fue posible capturar el texto. ${e}");
                    assert false, "No fue posible capturar el texto. ${e}"
                }
            break      
        }
    }

    public void ingresarValorCapturado(def params) {
        WebElement element = Elements.find(params,Navegador.Driver)
        try{
            element.clear();
        	element.sendKeys(Valor);
            println("Se ha ingresado el texto ${Valor} en un elemento. ");
        }catch(Exception e){
            println("No fue posible Ingresar el texto capturado. ${e}");
            assert false, "No fue posible Ingresar el texto capturado. ${e}" 
        }
    }
    
    public void ExportarValor(def params){
        long timeStamp = System.currentTimeMillis();
        String ruta = params."Ruta"+params."Nombre Archivo"+timeStamp+".txt"
        File archivo = new File(ruta);
        BufferedWriter bw;
        if(archivo.exists()) {
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write(Valor);
            println("Se ha creado un archivo de texto llamado ${ruta} que contiene el texto capturado.");
        } else {
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write(Valor);
            println("Se ha creado un archivo de texto llamado ${ruta} que contiene el texto capturado.");
        }
        bw.close();
    }
    
}