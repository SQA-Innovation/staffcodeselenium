package actions
import actions.selenium.Navegador

class CambiaraAFramePrincipal{
  	public void run(def params){
  		try{
          	Navegador.Driver.switchTo().defaultContent()
          	println("Se han cambiado al frame principal.");
      	}catch(Exception e){
          	println("No se pudo cambiar al frame principal. "+e);
          	assert false, "No se pudo cambiar al frame principal. ${e}"
      	}
  	}
}