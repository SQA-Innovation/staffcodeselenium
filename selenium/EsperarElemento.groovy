package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement

class EsperarElemento{
    public void run (def params){
        
        int count = params."Timeout En Segundos".toInteger()
        while(count >= 0){
            def elements = Elements.findAll(params,Navegador.Driver)
            if(elements.size() > 0) break
            sleep(1000)
            count--
        }
        if(count <= 0){
            println("El elemento no fue encontrado en "+params."Timeout En Segundos"+" segundos.");
            assert false,"El elemento no fue encontrado en ${params."Timeout En Segundos"} segundos."
        }
        println("El elemento fue encontrado con exito.");
    }
}