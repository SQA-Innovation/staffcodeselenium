package actions.selenium

import actions.selenium.Navegador
import org.openqa.selenium.JavascriptExecutor

class EjecutarJavascript{
  
	public void run(def params){
		def js = (JavascriptExecutor) Navegador.Driver
	    try {
	    	js.executeScript(params.Codigo)
	    	println("Se ejecuto codigo JavaScript en el sitio.");	
	    }
	    catch(Exception e) {
	    	println("No se pudo ejecutar codigo JavaScript en el sitio. "+e);
	    	assert false, "No se pudo ejecutar codigo JavaScript en el sitio. ${e}"
	    }
	    
	}
}