package actions.selenium;


import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.JavascriptExecutor
import java.util.List

class DeseleccionarItem{
  
    public void DeseleccionarTexto(def params){ 
        WebElement element = Elements.find(params,Navegador.Driver)
        try{
            new Select(element).deselectByVisibleText(params."Texto Visible")
            println("Se ha deseleccionado el elemento con el texto "+params."Texto Visible"+" y el ID "+params."ID"+". "+e);
        }catch(Exception e){
            println("Se ha deseleccionado el elemento con el texto "+params."Texto Visible"+" y el ID "+params."ID"+". "+e);
            assert false,"No se pudo deseleccionar el elemento con el texto ${params."Texto Visible"} y el ID ${params."ID"}. ${e}"
        }  
    }
    
    public void DeseleccionarValue(def params){
        WebElement element = Elements.find(params,Navegador.Driver)
        try{
            new Select(element).deselectByValue(params."Value")
            println("Se ha deseleccionado el elemento con el valor "+params."Texto Visible"+" y el ID "+params."ID"+". "+e);
        }catch(Exception e){
            println("No se pudo deseleccionar el elemento con el valor "+params."Texto Visible"+" y el ID "+params."ID"+". "+e);
            assert false,"No se pudo deseleccionar el elemento con el valor ${params."Value"} y el ID ${params."ID"}. ${e}"
        }  
    }
    
    public void DeseleccionarIndex(def params){
        WebElement element = Elements.find(params,Navegador.Driver)
        try{
            new Select(element).deselectByIndex(params.Index.toInteger())
            println("Se ha deseleccionado el elemento con el index "+params."Texto Visible"+" y el ID "+params."ID"+". "+e);
        }catch(Exception e){
            println("No se pudo deseleccionar el elemento con el index "+params."Texto Visible"+" y el ID "+params."ID"+". "+e);
            assert false,"No se pudo deseleccionar el elemento con el index ${params.Index.toInteger()} y el ID ${params."ID"}. ${e}"
        }  
    }
    
    public void DeseleccionarTodos(def params){
      	try {
            Select oLista = Elements.isListaPresente(params,Navegador.Driver)
            oLista.deselectAll();    
            println("Se han deseleccionado todos los elementos");  
        }
        catch(Exception e) {
            println("No se pudieron deseleccionar todos los elementos. "+e);
            assert false,"No se pudieron deseleccionar todos los elementos. ${e}"
        }
    }
}