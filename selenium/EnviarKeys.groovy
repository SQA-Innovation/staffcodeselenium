package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement
import org.openqa.selenium.Keys

class EnviarKeys{
  
    public static void run(def params){
        WebElement element = Elements.find(params,Navegador.Driver)
        
        if(params.Texto != null){
            try {
                element.sendKeys(params."Texto")
                println("Se ha ingresado el texto "+params."Texto"+" a un elemento."); 
            }
            catch(Exception e) {
                println("No se pudo ingresar el texto "+params."Texto"+" a un elemento. "+e);
                assert false,"No se pudo ingresar el texto ${params."Texto"} a un elemento. ${e}"  
            }      
        }
        
        if(params.Key != null){
            try {
                Binding binding = new Binding()
                binding.setVariable("key", null)
                GroovyShell shell = new GroovyShell(binding)
                shell.evaluate("key = org.openqa.selenium.Keys.${params.Key}")
                element.sendKeys(binding.getVariable("key"))   
                println("Se ha ingresado la key "+params.Key+" a un elemento.");
            }
            catch(Exception e) {
                println("No se pudo ingresar la key "+params.Key+" al elemento.");
                assert false,"No se pudo ingresar la key ${params.Key} al elemento."
            }
            
        }
    
    }
}