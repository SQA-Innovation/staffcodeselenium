package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.JavascriptExecutor
import java.util.List

class ConfigurarCheckBox{
  
	public static void Marcar(def params){
	    WebElement element = Elements.find(params,Navegador.Driver)
	    try{
	        if(element.selected == false){
	            JavascriptExecutor executor = (JavascriptExecutor) Navegador.Driver
	            executor.executeScript("arguments[0].click();", element)
	            println("Se ha marcado un checkbox");
	        }
	    }catch(Exception e){
	    	println("El elemento no pudo ser marcado. "+e);
	        assert false,"El elemento no pudo ser marcado. ${e}"
	    }
	    
	}
    
    public static void Desmarcar(def params){
        WebElement element = Elements.find(params,Navegador.Driver)
        try{
            if(element.selected == true){
                JavascriptExecutor executor = (JavascriptExecutor) Navegador.Driver
                executor.executeScript("arguments[0].click();", element)
                println("Se ha desmarcado un checkbox");
            }
        }catch(Exception e){
        	println("El elemento no pudo ser desmarcado. "+e);
            assert false,"El elemento no pudo ser desmarcado. ${e}"
        }
    }
    
    
    
   public void marcarTodosCheckbox(def params){
		List<WebElement> checkbox = Elements.findAll(params,Navegador.Driver)
		if (checkbox.size()>0){
			for (WebElement element : checkbox) {
				if (!element.isSelected()) {
					element.click();
				}
			}
			println("Se marcaron todos los Checkbox indicados.");
		}else{
			println("No se encontraron elementos tipo Checkbox. "+e);
            assert false,"No se encontraron elementos tipo Checkbox. ${e}"
		}
	}
    
    
    
   public void desmarcarTodosCheckbox(def params){
		List<WebElement> checkbox = Elements.findAll(params,Navegador.Driver)
		if (checkbox.size()>0){
			for (WebElement element : checkbox) {
				if (element.isSelected()) {
					element.click()
				}
			}
			println("Se desmarcaron todos los Checkbox indicados.");
			//log.info("Se desmarcaron todos los Checkbox indicados");
		}else{
			println("No se encontraron elementos tipo Checkbox. "+e);
            assert false,"No se encontraron elementos tipo Checkbox. ${e}"
		}
   }
}