package actions.selenium;


import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select
import java.util.List

class SeleccionarItem{
    
    public void SeleccionarTexto(def params){
        WebElement element = Elements.find(params,Navegador.Driver)
        try{
            new Select(element).selectByVisibleText(params."Texto Visible")
            println("Se ha seleccionado el Item ${params."Texto Visible"} en el elemento ${params."Tipo ID"}:${params.ID}");
        }catch(Exception e){
            println("No se pudo seleccionar el elemento con el texto ${params."Texto Visible"} y el ID ${params."ID"}.");
            assert false,"No se pudo seleccionar el elemento con el texto ${params."Texto Visible"} y el ID ${params."ID"}. ${e}"
      	} 
    }
    
    public void SeleccionarValue(def params){
        WebElement element = Elements.find(params,Navegador.Driver)
        try{
            new Select(element).selectByValue(params."Value")
            println("Se ha seleccionado el Item ${params."Value"} en el elemento ${params."Tipo ID"}:${params.ID}");
        }catch(Exception e){
            println("No se pudo seleccionar el elemento con el texto ${params."Value"} y el ID ${params."ID"}.");
            assert false,"No se pudo seleccionar el elemento con el value ${params."Value"} y el ID ${params."ID"}. ${e}"
      	} 
    }
    
    public void SeleccionarIndex(def params){
        WebElement element = Elements.find(params,Navegador.Driver)
        try{
            new Select(element).selectByIndex(params.Index.toInteger())
            println("Se ha seleccionado el Item ${params.Index.toInteger()} en el elemento ${params."Tipo ID"}:${params.ID}");
        }catch(Exception e){
            println("No se pudo seleccionar el elemento con el texto ${params.Index.toInteger()} y el ID ${params."ID"}.");
            assert false,"No se pudo seleccionar el elemento con el Index ${params.Index.toInteger()} y el ID ${params."ID"}. ${e}"
      	} 
    }
    
    public void SeleccionarTodos(def params){
		Select oLista = Elements.isListaPresente(params, Navegador.Driver)
		List<WebElement> oitems = oLista.getOptions()
		int iCantidadElementos = oitems.size()
		
		if(oLista.isMultiple()){ //Si la lista es multiple permite seleccionar todos los elementos	
			for(int i =0; i < iCantidadElementos ; i++){
				oLista.selectByIndex(i)
			}
			println("Se seleccionaron todos los elementos de la lista");
		}
	}
}