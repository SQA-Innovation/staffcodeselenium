package actions.selenium
import actions.selenium.Navegador

class NavegaraURL{
  
  	public void run(def params){
    	try {
    		Navegador.Driver.navigate().to(params.URL)
    		println("Se ha accedido a la URL: ${params.URL}.");	
    	}
    	catch(Exception e) {
    		println("No se pudo acceder a la URL: ${params.URL}. ${e}");
    		assert false,"No se pudo acceder a la URL: ${params.URL}. ${e}"
    	}

  	}
}