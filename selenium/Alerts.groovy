package actions.selenium;

import org.openqa.selenium.Alert
import org.openqa.selenium.NoAlertPresentException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.WebDriverWait

import actions.selenium.Navegador

class Alerts{
    public Alert capturarAlert(int tiempo){
       int i=0
	   Alert alert = null
	   boolean isPresente = false
	   while(i++<tiempo)
	   {
	        try{
	            alert = Navegador.Driver.switchTo().alert()
	            isPresente = true
	            break
	        }catch(NoAlertPresentException e){
		          try {
					Thread.sleep(1000)
				} catch (InterruptedException e1) {
					e1.printStackTrace()
			}
	          continue
	        }
	   }
	   
	    if (!isPresente){
	   		println("No se encontró ningún mensaje de alerta.");
		    assert false, "No se encontró ningún mensaje de alerta."
	    }
	   return alert
    }
    
    public void compararTexto(def params){
        Alert alerta = capturarAlert(10)
		String TextoActual = alerta.getText()
		
		if (TextoActual.contentEquals(params."Texto")){
			println("El texto esperado '${params."Texto"}' coincide con el texto en el alert '${TextoActual}'.");
			assert true, "Los textos coinciden."
        }else{
        	println("El texto esperado '${params."Texto"}' no coincide con el texto en el alert '${TextoActual}'.");
            assert false, "El texto esperado '${params."Texto"}' no coincide con el texto en el alert '${TextoActual}'."
        	//String Mensaje = "El mensaje \"" + alerta.getText() + "\"" + " del alert no coincide con: \""+Texto+"\"";
        }
    }
    
    public void clickAlert(def params){
		Alert alerta = capturarAlert(10)
		
		if (params."Accion" == "Aceptar"){
			alerta.accept();
			println("Clic en el botón aceptar del alert.");
            assert true, "Clic en el botón aceptar del alert."
		}else{
			alerta.dismiss();
			println("Clic en el botón cancelar del alert.");
            assert true, "Clic en el botón cancelar del alert."
		}
	}
    
    public void ingresarTextoPrompt(def params) {
        try{
            Alert alerta = capturarAlert(10)
            alerta.sendKeys(params."Texto")
            alerta.accept()
            println("se ingreso el texto ${params."Texto"} en el prompt");
        }catch(Exception e){
            println("No se pudo ingresar el texto ${params."Texto"} en el prompt." + e);
            assert false, "No se pudo ingresar el texto ${params."Texto"} en el prompt. ${e}"
        }
	}
}