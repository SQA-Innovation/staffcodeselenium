package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import actions.selenium.ClickMouse

import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.JavascriptExecutor

import java.awt.Robot
import java.awt.event.InputEvent

class ClickRobot{
  
  public void run(def params){

    WebElement element = Elements.find(params,Navegador.Driver)
          
    int x = element.getLocation().getX();
	int y = element.getLocation().getY();
		
	x = x + 50;
	y = y +105;
	     
	try {
		Robot robot = new Robot()
		robot.mouseMove(x, y)
		robot.delay(1000)
		robot.mousePress(InputEvent.BUTTON1_MASK)
		robot.delay(200)
		robot.mouseRelease(InputEvent.BUTTON1_MASK)
		robot.delay(200)
		println("Se ha hecho click en el elemento "+params."Tipo ID"+":"+params.ID);
    } catch (Exception e) {
    	println("No se pudo hacer click en el elemento. "+e);
        assert false, "No se pudo hacer click sobre el elemento. ${e}"
    }          
  }
}