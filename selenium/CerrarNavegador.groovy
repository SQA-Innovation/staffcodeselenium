package actions.selenium
import actions.selenium.Navegador

class CerrarNavegador{
  	public void run(def params){
    	if(Navegador.Driver){
    		Navegador.Driver.quit()
    		println("Se ha cerrado el driver de selenium.");
    	}else{
    		println("Al parecer el driver ya esta cerrado.");
    		assert false,"Al parecer el driver ya esta cerrado."
    	}
  	}
}