package actions.selenium

import actions.selenium.Navegador

class ObtenerValordeCookie{
  
  public def run(def params){
    def cookie = Navegador.Driver.manage().getCookies().find{it.getName() == params.Nombre}
    assert cookie != null, "Error cookie no encontrada."
    return cookie.getValue()
  }
}
