package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

class ClickMouse{
  
 	public void run(def params){
	    WebElement element = Elements.find(params,Navegador.Driver)
	    Actions action = new Actions(Navegador.Driver)
	   	try {
	   	 	action.moveToElement(element).click(element).build().perform() 
	   	 	println("Se ha hecho click mouse en el elemento "+params."Tipo ID"+":"+params.ID);
	   	}
	   	catch(Exception e) {
	   	 	println("No se pudo realizar click mouse sobre un elemento." +e);
    		assert false, "No se pudo realizar click mouse sobre un elemento. ${e}"
	   	}  
  	}
}