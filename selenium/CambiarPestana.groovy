package actions.selenium;

import actions.selenium.Navegador

import java.util.ArrayList
import java.util.List
import java.util.Set

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.OutputType
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

class CambiarPestana{
    
    public static String ventanaPadre
    
    public void run(def params){
		ventanaPadre = Navegador.Driver.getWindowHandle();
		ArrayList<String> Pestanas = new ArrayList<String> (Navegador.Driver.getWindowHandles())
		
		if (Pestanas.size() > 1){
            try{
                Navegador.Driver.switchTo().window(Pestanas.get(0))
                println("Se han cambiado de pestaña.");
            }catch(Exception e){
                println("No se pudo cambiar el foco.");
                assert false,"No se pudo cambiar el foco"
            }
		}else{
            println("No hay otras pestañas abiertas para cambiar el foco.");
            assert false,"No hay otras pestañas abiertas para cambiar el foco"
		}
	}

	public void CambiarPestanas(def params){
        try{
            ArrayList<String> tabs = new ArrayList<String> (Navegador.Driver.getWindowHandles());
			Integer pestana = params.Indicador.toInteger();
            Navegador.Driver.switchTo().window(tabs.get(pestana-1));
            println("Se ha cambiado a la pestaña numero ${pestana} en el navegador");
        }catch(Exception e){
            println("No se pudo crear una nueva pestaña. "+e);
            assert false,"No se pudo crear una nueva pestaña. ${e}"
        }
    }
}