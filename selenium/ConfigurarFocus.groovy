package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

class ConfigurarFocus{
  	public void run(def params){
	    WebElement element = Elements.find(params,Navegador.Driver)
	    try {
	    	if(element.getTagName() == "input"){
		       element.sendKeys("");
		    } 
		    else{
		       new Actions(driver).moveToElement(element).perform();
		    }	
		    println("Se ha cambiado el foco a otro elemento.");
	    }
	    catch(Exception e) {
	    	println("No se pudo cambiar el foco a otro elemento. "+e);
            assert false,"No se pudo cambiar el foco a otro elemento. ${e}"
	    }
	    
  	}
}