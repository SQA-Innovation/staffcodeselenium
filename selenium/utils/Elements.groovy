package actions.selenium.utils

import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.support.ui.Select

import java.awt.Dimension
import java.awt.Rectangle
import java.awt.Robot
import java.awt.Toolkit
import java.awt.image.BufferedImage
import java.io.File
import java.io.IOException
import java.util.List
import java.util.Set
import javax.imageio.ImageIO


class Elements{
  
  public static WebElement find(def params,WebDriver Driver){
    assert Driver != null, "Error, el navegador no fue abierto. Usa la acción Abrir Navegador."
	def js = (JavascriptExecutor) Driver
    WebElement foundElement = null
    switch (params."Tipo ID"){
      case "Class Name":
      	foundElement = Driver.findElement(By.className(params.ID))
        resaltar(foundElement, Driver)
       	break
      case "Css Selector":
      	foundElement = Driver.findElement(By.cssSelector(params.ID))
        resaltar(foundElement, Driver)
      	break
      case "ID":
        for(int i=0;i<10;i++){
			try{
				foundElement = Driver.findElement(By.id(params.ID)) 
			}
			catch(Exception e){
				sleep(1000)
			}
		}
        resaltar(foundElement, Driver)
      	break      
      case "Link Text":
      	foundElement = Driver.findElement(By.linkText(params.ID))
        resaltar(foundElement, Driver)
      	break      
      case "XPath":
        for(int i=0;i<10;i++){
			try{
				foundElement = Driver.findElement(By.xpath(params.ID))
			}
			catch(Exception e){
				sleep(1000)
			}
		}
        resaltar(foundElement, Driver)
      	break      
      case "Name":
      	foundElement = Driver.findElement(By.name(params.ID))
        resaltar(foundElement, Driver)
      	break      
      case "Partial Link Text":
      	foundElement = Driver.findElement(By.partialLinkText(params.ID))
        resaltar(foundElement, Driver)
      	break      
      case "Tag Name":
      	foundElement = Driver.findElement(By.tagName(params.ID))
        resaltar(foundElement, Driver)
      	break
      default:
        foundElement = Driver.findElement(By.id(params.ID))
        resaltar(foundElement, Driver)
    }
    
    return foundElement
  }
  
  public static def findAll(def params,WebDriver Driver){
    assert Driver != null, "Error, el navegador no fue abierto. Usa la acción Abrir Navegador."
    
    def foundElements = []
    
    switch (params."Tipo ID"){
      case "Class Name":
      	foundElements = Driver.findElements(By.className(params.ID))
      	break
      case "Css Selector":
      	foundElements = Driver.findElements(By.cssSelector(params.ID))
      	break
      case "ID":
      	foundElements = Driver.findElements(By.id(params.ID))
      	break      
      case "Link Text":
      	foundElements = Driver.findElements(By.linkText(params.ID))
      	break      
      case "XPath":
      	foundElements = Driver.findElements(By.xpath(params.ID))
      	break      
      case "Name":
      	foundElements = Driver.findElements(By.name(params.ID))
      	break      
      case "Partial Link Text":
      	foundElements = Driver.findElements(By.partialLinkText(params.ID))
      	break      
      case "Tag Name":
      	foundElements = Driver.findElements(By.tagName(params.ID))
      	break
      default:
        foundElements = Driver.findElements(By.id(params.ID))
    }
    
    return foundElements
  }
    
  public static Select isListaPresente(def params,WebDriver Driver){
    assert Driver != null, "Error, el navegador no fue abierto. Usa la acción Abrir Navegador."
      
    def js = (JavascriptExecutor) Driver 
    By locator
	locator = localizador(params."Tipo ID", params.ID, Driver)      
	Select oLista = null
	boolean isPresente = false	
      
	for(int i=0;i<10;i++){
		try{
			WebElement element = Driver.findElement(locator)
            resaltar(element, Driver)
			oLista = new Select(element)
			isPresente = true
			break;
		}
		catch(Exception e){
			try {
				Thread.sleep(1000)
			} catch (InterruptedException e1) {
				System.out.println("Esperando que el elemento asperezca");
			}
		}
	}
		
	if (isPresente){
		assert true, "Lista encontrada"
	}else{
        assert false, "Se ha producido un error: La lista: '${params.ID}' no puedo ser localizada."
	}
	return oLista;
    
    
  }
    
  public static By localizador(String tipoLocalizador, String valor, Driver) {
		By by
		switch (tipoLocalizador) {
		case "ID":
			by = By.id(valor)
			break
		case "Name":
			by = By.name(valor)
			break
		case "Class Name":
			by = By.className(valor)
			break
		case "Xpath":
			by = By.xpath(valor)
			break
		case "CSS":
			by = By.cssSelector(valor)
			break
		case "Link Text":
			by = By.linkText(valor)
			break
		case "Partial Link Text":
			by = By.partialLinkText(valor)
			break
		case "Tag Name":
			by = By.tagName(valor)
			break
		default:
			by = null
			break
		}
		return by
  }
    
  public static void resaltar(def params, WebDriver Driver){
        def js = (JavascriptExecutor) Driver
		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", params)

		try {
			Thread.sleep(500)
		} 
		catch (InterruptedException e) {
            assert false,"${e}"
		} 

		js.executeScript("arguments[0].setAttribute('style','border: solid 2px white');", params)
    }
}