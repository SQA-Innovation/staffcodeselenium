package actions.selenium

import actions.selenium.Navegador

class IrAdelante{
	public void run(def params){
	    try {
	    	Navegador.Driver.navigate().forward()
	    	println("El navegador ha ido a la pagina siguiente");
	    }
	    catch(Exception e) {
	    	println("No se puede saltar a la pagina siguiente. "+e);
	    	assert false, "No se puede saltar a la pagina siguiente. ${e}"
	    }
	}
}