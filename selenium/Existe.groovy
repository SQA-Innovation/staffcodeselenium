package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement

class Existe{
	public void run (def params){

	    def elements = Elements.findAll(params,Navegador.Driver)
	    if(params."Numero de coincidencias"){
	    	if (elements.size() != params."Numero de coincidencias".toInteger()) {
	    		println("Error con el Elemento: "+params.ID+" No se encontro el numero esperado de coincidencias: "+params."Numero de coincidencias"+".  Fueron encontrados: "+elements.size()+" veces.");
	    		assert false,"Error con el Elemento: ${params.ID} No se encontro el numero esperado de coincidencias: ${params."Numero de coincidencias"}.  Fueron encontrados: ${elements.size()} veces."
	    	}else{
	    		println("Se encontro el numero esperado de coincidencias.");
	    	}
	    }
	    else{
	    	println("Error en el Elemento: "+elements.size()+" fueron encontrados.");
	        assert elements.size() > 0,"Error en el Elemento: ${elements.size()} fueron encontrados."
	    }
	}
}