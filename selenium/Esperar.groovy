package actions.general

class Esperar{
  
	public run(def params){
	    if(params.Segundos){
	    	sleep(params.Segundos.toInteger() * 1000) 
	    	println("Se ha realizado una espera de "+params.Segundos+"segundos."); 
	    }    
	}
}