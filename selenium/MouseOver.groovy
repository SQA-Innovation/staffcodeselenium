package actions.selenium

import org.openqa.selenium.JavascriptExecutor
import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement

class MouseOver{

	public void run(def params){
	    WebElement element = Elements.find(params,Navegador.Driver)
	    JavascriptExecutor jsExec = (JavascriptExecutor) Navegador.Driver
	    String javaScript = "var evObj = document.createEvent('MouseEvents');evObj.initEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);arguments[0].dispatchEvent(evObj);"
	    try {
	    	jsExec.executeScript(javaScript, element)
	    	println("Se ha colocado el mouse sobre un elemento.");	
	    }
	    catch(Exception e) {
	    	println("No se pudo colocar el mouse sobre un elemento. " +e);
	    	assert false,"No se pudo colocar el mouse sobre un elemento. ${e}"
	    }
	}
}