package actions.selenium;

import actions.selenium.Navegador

import java.io.File;
import java.io.IOException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.OutputType;
import org.apache.commons.io.FileUtils;

class CapturarPantalla{
    public void capturarScreenShot(def params){
        String ruta = params."Ruta"
        String nombre = params."Nombre"
        try {
			File src= ((TakesScreenshot)Navegador.Driver).getScreenshotAs(OutputType.FILE);
			long timeStamp = System.currentTimeMillis();
			String fileName = ruta+nombre+timeStamp+".png";
			FileUtils.copyFile(src, new File(fileName));
			println("Se ha hecho una captura de pantalla llamada "+nombre+timeStamp+".png");
		} catch (IOException e){
			println("No fue posible capturar la pantalla");
            assert false,"No fue posible capturar la pantalla"
		}
    }
}