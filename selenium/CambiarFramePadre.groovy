package actions.selenium;
import actions.selenium.Navegador

class CambiarFramePadre{
    public void run(def params){
        try {
        	Navegador.Driver.switchTo().parentFrame()
        	println("Se han cambiado al frame padre.");	
        }
        catch(Exception e) {
        	println("No se pudo cambiar al frame padre. " +e);
        	assert false,"No se pudo cambiar al frame padre. ${e}"
        }

    }
}