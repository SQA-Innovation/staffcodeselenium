package actions.selenium;

import actions.selenium.Navegador
import actions.selenium.utils.Elements
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import java.util.concurrent.TimeUnit

class Desplazamiento{
    
    public void Arriba(def params){
        JavascriptExecutor js = (JavascriptExecutor) Navegador.Driver
		try {
            js.executeScript("scroll(0, -250);");
            println("Se ha ido al inicio de la pagina.");      
        }
        catch(Exception e) {
            println("No se pudo ir al inicio de la pagina. "+e);
            assert false,"No se pudo ir al inicio de la pagina. ${e}"
        }
    }
    
    public void Abajo(def params){
        JavascriptExecutor js = (JavascriptExecutor) Navegador.Driver
        try {
            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
            println("Se ha ido al fin de la pagina.");      
        }
        catch(Exception e) {
            println("No se pudo ir al fin de la pagina. "+e);
            assert false,"No se pudo ir al fin de la pagina. ${e}"
        }
    }
    
    public void scrollPagina(def params){
        try{
            WebElement element = Elements.find(params,Navegador.Driver)
            JavascriptExecutor js = (JavascriptExecutor) Navegador.Driver
            int x = element.getLocation().getX();
            int y = element.getLocation().getY();
            js.executeScript("window.scrollBy(" + x + "," + y + ")", "");
            println("Se ha realizado scroll en la pagina."); 
        }catch(Exception e){ 
            println("Se ha producido un error: Debe elegir un tipo de Scroll válido. "+e); 
            assert false,"Se ha producido un error: Debe elegir un tipo de Scroll válido. ${e}"
        }
    }

	public void scrollElemento(def params){
        try{
            WebElement element = Elements.find(params,Navegador.Driver)
            JavascriptExecutor js = (JavascriptExecutor) Navegador.Driver
            js.executeScript("arguments[0].scrollIntoView(true);",element);
            println("Se ha realizado scroll en la pagina."); 
        }catch(Exception e){ 
            println("Se ha producido un error: Debe elegir un tipo de Scroll válido. "+e); 
            assert false,"Se ha producido un error: Debe elegir un tipo de Scroll válido. ${e}"
        }
    }
    
}