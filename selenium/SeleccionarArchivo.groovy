package actions.selenium;

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import actions.selenium.ClickMouse

import java.awt.Robot
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection
import java.awt.event.KeyEvent
import java.awt.event.InputEvent

import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.JavascriptExecutor

class SeleccionarArchivo{
    public void run(def params){
        try{
            WebElement element = Elements.find(params,Navegador.Driver)
            element.sendKeys(params."Ubicacion")
            println("Se ha cargado un archivo con exito. ");
        }catch(Exception e){
            println("El archivo no pudo ser cargado. ${e}");
            assert false,"El archivo no pudo ser cargado. ${e}"
        }    
    }
    
    
    public void subirArchivo(def params){
        WebElement element = Elements.find(params,Navegador.Driver)
		element.click();
		StringSelection stringSelection  = new StringSelection(params."Ubicacion");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection , null);
		try {
            Robot robot = new Robot();
            Thread.sleep(1000);
            
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.delay(150);
            robot.keyRelease(KeyEvent.VK_ENTER);
            println("Se ha cargado un archivo con exito. ");
        } catch (Exception e) {
        	println("El archivo no pudo ser cargado. ${e}");
            assert false,"El archivo no pudo ser cargado. ${e}"
        }        
    }
}