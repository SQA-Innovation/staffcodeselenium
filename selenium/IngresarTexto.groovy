package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement
import org.openqa.selenium.Keys
import org.openqa.selenium.JavascriptExecutor

class IngresarTexto{
  
    public static void run(def params){
        WebElement element = Elements.find(params,Navegador.Driver)
        try {
            element.clear()
            element.sendKeys(params."Texto")  
            println("Se ha ingresado el texto "+params."Texto"+" en un elemento.");    
        }
        catch(Exception e) {
            println("No se ha podido ingresar el texto "+params."Texto"+" en un elemento. "+e);
            assert false,"No se ha podido ingresar el texto "+params."Texto"+" en un elemento. ${e}"   
        }       
    }
    
    public static void ingresarTextoCampoHidden(def params){
	WebElement element = Elements.find(params,Navegador.Driver)
	JavascriptExecutor js = (JavascriptExecutor) Navegador.Driver
        try{
            switch (params."Tipo ID"){
                case "ID":
                    try {
                        js.executeScript("document.getElementById('${params.ID}').setAttribute('type', 'text');")
                        element.clear()
                        element.sendKeys(params."Texto")
                        js.executeScript("document.getElementById('${params.ID}').setAttribute('type', 'hidden');") 
                        println("Se ha ingresado el texto "+params."Texto"+" en un elemento oculto.");  
                    }
                    catch(Exception e) {
                        println("No se ha podido ingresar el texto "+params."Texto"+" en un elemento oculto. "+e);
                        assert false,"No se ha podido ingresar el texto "+params."Texto"+" en un elemento oculto. ${e}"
                    }
                break
                case "Tag Name":
                  js.executeScript("document.getElementsByTagName('${params.ID}')[${params.Index.toInteger()}].setAttribute('type', 'text');")
                  js.executeScript("document.getElementsByTagName('${params.ID}')[${params.Index.toInteger()}].value='${params."Texto"}';")
                  js.executeScript("document.getElementsByTagName('${params.ID}')[${params.Index.toInteger()}].setAttribute('type', 'hidden');")
                break
                default:
                    try {
                        js.executeScript("document.getElementById('${params.ID}').setAttribute('type', 'text');")
                        element.clear()
                        element.sendKeys(params."Texto")
                        js.executeScript("document.getElementById('${params.ID}').setAttribute('type', 'hidden');") 
                        println("Se ha ingresado el texto "+params."Texto"+" en un elemento oculto.");  
                    }
                    catch(Exception e) {
                        println("No se ha podido ingresar el texto "+params."Texto"+" en un elemento oculto. "+e);
                        assert false,"No se ha podido ingresar el texto "+params."Texto"+" en un elemento oculto. ${e}"
                    }
            }   
        }catch(Exception e){
            println("El elemento con el identificador "+params."Tipo ID"+":"+params.ID+" no ha sido localizado. "+e);
            assert false,"El elemento con el identificador '${params."Tipo ID"}:${params.ID}' no ha sido localizado. ${e}"
        }    
    }
  
    public static void LimpiarCampo(def params){
        WebElement element = Elements.find(params,Navegador.Driver)
        try {
            element.clear() 
            println("Se ha limpiado un elemento");      
        }
        catch(Exception e) {
            println("No se pudo limpiar el elemento. "+e);  
            assert false, "No se pudo limpiar el elemento. ${e}"  
        }    
    }
}