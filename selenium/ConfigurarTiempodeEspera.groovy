package actions.selenium

import actions.selenium.Navegador

class ConfigurarTiempodeEspera{
  
  	public void run(def params){
    	try {
    		Navegador.Driver.manage().timeouts().implicitlyWait(params.Segundos.toInteger(), java.util.concurrent.TimeUnit.SECONDS)
    		println("Se ha configurado el tiempo de espera a "+params.Segundos+" segundos");	
    	}
    	catch(Exception e) {
    		println("No se pudo configurar el tiempo de espera a "+params.Segundos+" segundos. "+e);
            assert false,"No se pudo configurar el tiempo de espera a "+params.Segundos+" segundos. ${e}"
    	}
    	
  	}
}