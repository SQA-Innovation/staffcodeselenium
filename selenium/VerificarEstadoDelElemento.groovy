package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement

class VerificarEstadoDelElemento{
  
    public void run(def params){

        WebElement element = Elements.find(params,Navegador.Driver)
    	
        if(params."Esta Habilitado" != null){
            if (element.isEnabled() != params."Esta Habilitado") {
                println("Error, el estado esperado como: ${params."Esta Habilitado"} no coincide con el estado actual del elemento.");
                assert false, "Error, el estado esperado como: ${params."Esta Habilitado"} no coincide con el estado actual del elemento."
            }else{
                println("El estado esperado coincide con el estado actual del elemento.");
            }
        }
        
        if(params."Esta Visible" != null){
            if (element.isDisplayed() != params."Esta Visible") {
                println("Error, la visibilidad esperada como: ${params."Esta Visible"} no coincide con el estado actual del elemento.");
                assert false, "Error, la visibilidad esperada como: ${params."Esta Visible"} no coincide con el estado actual del elemento."
            }else{
                println("El estado esperado coincide con el estado actual del elemento.");
            }
        }
    }
}