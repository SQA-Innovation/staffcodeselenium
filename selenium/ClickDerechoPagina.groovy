package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

class ClickDerechoPagina{
  
	public void run(def params){
	    Actions action = new Actions(Navegador.Driver);
		try {
    		action.contextClick().perform()	
    		println("Se ha realizado click derecho sobre un elemento.");
    	}
    	catch(Exception e) {
    		println("No se pudo realizar click derecho sobre un elemento." +e);
    		assert false, "No se pudo realizar click derecho sobre un elemento. ${e}"
    	}
	}
}