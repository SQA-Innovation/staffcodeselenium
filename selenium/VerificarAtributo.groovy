package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement

class VerificarAtributo{
  
	public void run(def params){
	    WebElement element = Elements.find(params,Navegador.Driver)
	    if (!params.Value.equals(element.getAttribute(params."Nombre Atributo"))) {
	    	println("Error: El valor ${element.getAttribute(params."Nombre Atributo")} de el atributo: ${params."Nombre Atributo"} no coincide con el valor esperado de: ${params.Value}");
	    	assert false, "Error: El valor ${element.getAttribute(params."Nombre Atributo")} de el atributo: ${params."Nombre Atributo"} no coincide con el valor esperado de: ${params.Value}"
	    }else{
	    	println("El valor ${element.getAttribute(params."Nombre Atributo")} de el atributo: ${params."Nombre Atributo"} coincide con el valor esperado de: ${params.Value}");
	    }   
	}
}
