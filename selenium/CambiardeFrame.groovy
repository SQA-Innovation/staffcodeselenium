package actions.selenium

import actions.selenium.Navegador
import org.openqa.selenium.NoSuchFrameException

class CambiardeFrame{
  
    public void run(def params){
        if(params.Nombre){
            try {
                Navegador.Driver.switchTo().frame(params.Nombre)
                println("Se ha cambiado de Frame a "+params.Nombre);
            }
            catch(Exception e) {
                println("No fue posible identificar el Frame "+params.Nombre);
                assert false,"No fue posible identificar el Frame ${params.Nombre}"
            }            
        }
        else if(params.Index){
            try {
                Navegador.Driver.switchTo().frame(params.Index.toInteger())
                println("Se ha cambiado de Frame con el indice "+params.Index);
            }
            catch(Exception e) {
                println("No fue posible identificar el Frame "+params.Index);
                assert false,"No fue posible identificar el Frame ${params.Index}"
            } 
        }
    }
    
    public void alternarFrame(def params) {
        if(params.Nombre){
            try{
		        Navegador.Driver.switchTo().defaultContent()
		        Navegador.Driver.switchTo().frame(params.Nombre)
                println("Se ha cambiado al Frame "+params.Nombre);
	        }catch(NoSuchFrameException e){
                println("No fue posible identificar el Frame "+params.Nombre);
                assert false,"No fue posible identificar el Frame: ${params.Nombre}"
	        }	
        }
        else if(params.Index){
            try{
		        Navegador.Driver.switchTo().defaultContent()
		        Navegador.Driver.switchTo().frame(params.Index.toInteger())
                println("Se ha cambiado al Frame "+params.Index);
	        }catch(NoSuchFrameException e){
                println("No fue posible identificar el Frame "+params.Index);
                assert false,"No fue posible identificar el Frame: ${params.Index.toInteger()}"
	        }	
        }	
    }
}