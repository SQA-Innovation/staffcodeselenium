package actions.selenium

import actions.selenium.utils.Elements
import actions.selenium.Navegador
import org.openqa.selenium.WebElement

class Click{
  
    public void run(def params){

        WebElement element = Elements.find(params,Navegador.Driver)
        int iTimeout = 20
        while(iTimeout > 0)
        try{
            element.click()
            println("Se ha hecho click en el elemento "+params."Tipo ID"+":"+params.ID);
            return
        }
        catch(org.openqa.selenium.WebDriverException err){
            iTimeout--
            if(err.message.contains("No se pudo hacer Click sobre el elemento.")){
                if(iTimeout == 0){
                    throw err
                }
            }
            else{
                throw err
            }
            sleep(1000)
        }
        println("No se pudo hacer Click sobre el elemento.");
    }
}