package actions.selenium

import actions.selenium.Navegador

import java.awt.Robot
import java.awt.Toolkit
import java.io.IOException
import java.util.List
import java.util.Set

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.OutputType
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement


class CerrarVentana{
  	public void run(def params){
    	try {
    		Navegador.Driver.close()	
    		println("Se ha cerrado una ventana.");
    	}
    	catch(Exception e) {
    		println("No se ha podido cerrar algo ventana.");
    		assert false,"No se ha podido cerrar algo ventana."
    	}
  	}
    
    
    //Cierra las ventanas abiertas diferentes a la ventana padre
 	public void cerrarVentana(def params){
		Set<String> Ventanas = Navegador.Driver.getWindowHandles()
		if (Ventanas.size() > 1){
			String VentanaSecundaria = Ventanas.iterator().next()
            try{
                cerrarVentanas(Navegador.Driver, VentanaSecundaria)
                println("Se ha cerrado una ventana.");
            }catch(Exception e){
            	println("No se pudo acceder al metodo.");
                assert false,"No se pudo acceder al metodo. ${e}"
            }
		} else {
			println("No hay otras ventanas/pestañas abiertas para cerrar");
            assert false,"No hay otras ventanas/pestañas abiertas para cerrar"
		}
	}
    
   // --
   public static boolean cerrarVentanas(WebDriver driver, String idVentanasAbiertas) {
		Set<String> idVentanas = driver.getWindowHandles()
		for (String idVentanaActual : idVentanas) {
			if (!idVentanaActual.equals(idVentanasAbiertas)) {
				driver.switchTo().window(idVentanaActual)
				driver.close()
				println("Se han cerrado todas las ventanas.");
			}
		}
		
		driver.switchTo().window(idVentanasAbiertas)
		if (driver.getWindowHandles().size() == 1)
			return true
		else
			return false
  }
}